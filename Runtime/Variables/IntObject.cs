using Sirenix.OdinInspector;
using UnityEngine;

namespace KristinaWaldt.ValueObjects
{
    [CreateAssetMenu(fileName = "IntObject", menuName = MenuPath + "Int")]
    public class IntObject : GenericValueObject<int>
    {
        [SerializeField]
        private bool hasMinValue;

        [SerializeField, ShowIf("hasMinValue")]
        private int minValue;

        protected override int AdjustValue(int value)
        {
            return hasMinValue ? Mathf.Max(minValue, value) : base.AdjustValue(value);
        }

        [Button]
        public void Add(int value)
        {
            RuntimeValue += value;
        }
		
        public void Add(IntObject value)
        {
            Add(value.RuntimeValue);
        }

        [Button]
        public void Subtract(int value)
        {
            RuntimeValue -= value;
        }
		
        public void Subtract(IntObject value)
        {
            Subtract(value.RuntimeValue);
        }
    }
}